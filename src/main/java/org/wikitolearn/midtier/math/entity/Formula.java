package org.wikitolearn.midtier.math.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_DEFAULT)
public class Formula {

  @JsonProperty("_id")
  private String id;

  @ApiModelProperty(readOnly = true)
  @JsonProperty("_etag")
  private String etag;

  @ApiModelProperty(readOnly = true)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE, dd MMM yyyy HH:mm:ss z")
  @JsonProperty("_updated")
  private Date updated;

  @ApiModelProperty(readOnly = true)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE, dd MMM yyyy HH:mm:ss z")
  @JsonProperty("_created")
  private Date created;

  @ApiModelProperty(readOnly = true)
  @JsonProperty("_deleted")
  private boolean deleted;

  private String formula;

  private SVG svg;

  @Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(Include.NON_DEFAULT)
  public static final class SVG {
    @JsonProperty("content_type")
    private String contentType;
    private int length;
    private String name;
    private String file;
  }
}

package org.wikitolearn.midtier.math.exception;

public class PdfBackendErrorException extends BackendErrorException {

  private static final long serialVersionUID = 872634560978350093L;
  
  public PdfBackendErrorException() {
    super();
  
  }
  public PdfBackendErrorException(String message, Throwable cause) {
      super(message, cause);
  }
  
  public PdfBackendErrorException(String message) {
      super(message);
  }
  
  public PdfBackendErrorException(Throwable cause) {
      super(cause);
  }
}

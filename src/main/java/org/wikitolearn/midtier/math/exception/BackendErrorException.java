package org.wikitolearn.midtier.math.exception;

public class BackendErrorException extends RuntimeException {

  private static final long serialVersionUID = -8522349828411452329L;
  
  public BackendErrorException() {
    super();
  
  }
  public BackendErrorException(String message, Throwable cause) {
      super(message, cause);
  }
  
  public BackendErrorException(String message) {
      super(message);
  }
  
  public BackendErrorException(Throwable cause) {
      super(cause);
  }
}

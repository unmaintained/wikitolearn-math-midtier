package org.wikitolearn.midtier.math.exception;

public class MathBackendErrorException extends BackendErrorException {

  private static final long serialVersionUID = 1688154335869912629L;
  
  public MathBackendErrorException() {
    super();
  
  }
  
  public MathBackendErrorException(String message, Throwable cause) {
      super(message, cause);
  }
  
  public MathBackendErrorException(String message) {
      super(message);
  }
  
  public MathBackendErrorException(Throwable cause) {
      super(cause);
  }
}

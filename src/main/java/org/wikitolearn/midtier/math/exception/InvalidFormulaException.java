package org.wikitolearn.midtier.math.exception;

public class InvalidFormulaException extends RuntimeException {

  private static final long serialVersionUID = -5675314018326222145L;
  
  public InvalidFormulaException() {
    super();
  
  }
  public InvalidFormulaException(String message, Throwable cause) {
      super(message, cause);
  }
  
  public InvalidFormulaException(String message) {
      super(message);
  }
  
  public InvalidFormulaException(Throwable cause) {
      super(cause);
  }
}

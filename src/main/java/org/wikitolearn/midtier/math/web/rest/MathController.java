package org.wikitolearn.midtier.math.web.rest;

import java.io.IOException;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wikitolearn.midtier.math.entity.ApiError;
import org.wikitolearn.midtier.math.entity.Formula;
import org.wikitolearn.midtier.math.entity.VerifyStatus;
import org.wikitolearn.midtier.math.entity.dto.in.StoreFormulaDto;
import org.wikitolearn.midtier.math.exception.BackendErrorException;
import org.wikitolearn.midtier.math.exception.InvalidFormulaException;
import org.wikitolearn.midtier.math.exception.MathBackendErrorException;
import org.wikitolearn.midtier.math.exception.ResourceNotFoundException;
import org.wikitolearn.midtier.math.service.FormulaService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/math")
public class MathController {

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private FormulaService formulasService;

  @ApiResponses({ @ApiResponse(code = 201, message = "Created", response = Formula.class),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
      @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class) })
  @ApiOperation(value = "Store a formula if it passes the internal validation")
  @PostMapping(value = "/formulas", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<?> storeFormula(@Valid @RequestBody final StoreFormulaDto formula)
      throws JsonParseException, JsonMappingException, InvalidFormulaException, BackendErrorException, IOException {
    final Formula storedFormula = formulasService.save(modelMapper.map(formula, Formula.class));
    final MultiValueMap<String, String> httpHeadersMap = new LinkedMultiValueMap<>();
    httpHeadersMap.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
    return handleResponse(storedFormula, httpHeadersMap, HttpStatus.CREATED);
  }

  @ApiResponses({ @ApiResponse(code = 200, message = "Ok", response = Formula.class),
      @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
      @ApiResponse(code = 404, message = "Not Found", response = ApiError.class) })
  @ApiOperation(value = "Get a formula JSON document given its id (SHA256 of the json encoded formula)")
  @GetMapping(value = "/formulas/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<?> getFormula(@PathVariable("id") final String id)
      throws ResourceNotFoundException, MathBackendErrorException {
    final Formula formula = formulasService.get(id);
    final MultiValueMap<String, String> httpHeadersMap = new LinkedMultiValueMap<>();
    httpHeadersMap.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
    return handleResponse(formula, httpHeadersMap, HttpStatus.OK);
  }

  @ApiResponses({ @ApiResponse(code = 200, message = "Ok"),
      @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
      @ApiResponse(code = 404, message = "Not Found", response = ApiError.class) })
  @ApiOperation(value = "Get a formula SVG given its id (SHA256 of the json encoded formula)."
      + " The endpoint returns with Content-Type image/svg+xml. Errors are returned as Content-Type application/json")
  @GetMapping(value = "/formulas/{id}/svg", produces = "image/svg+xml")
  public ResponseEntity<?> getFormulaSVG(@PathVariable("id") final String id)
      throws ResourceNotFoundException, MathBackendErrorException {
    final Resource svg = formulasService.getSVG(id);
    final MultiValueMap<String, String> httpHeadersMap = new LinkedMultiValueMap<>();
    httpHeadersMap.add(HttpHeaders.CONTENT_TYPE, "image/svg+xml");
    return handleResponse(svg, httpHeadersMap, HttpStatus.OK);
  }

  @ApiResponses({ @ApiResponse(code = 200, message = "Ok", response = VerifyStatus.class),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "Forbidden"),
      @ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
      @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class) })
  @ApiOperation(value = "Check formula validity")
  @PostMapping(value = "/check", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<?> checkFormula(@Valid @RequestBody final StoreFormulaDto formula)
      throws BackendErrorException, JsonParseException, JsonMappingException, IOException {
    final VerifyStatus verifyStatus = formulasService.check(modelMapper.map(formula, Formula.class));
    final MultiValueMap<String, String> httpHeadersMap = new LinkedMultiValueMap<>();
    httpHeadersMap.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
    return handleResponse(verifyStatus, httpHeadersMap, HttpStatus.OK);
  }

  private <T> ResponseEntity<T> handleResponse(final T body, final MultiValueMap<String, String> httpHeadersMap,
      final HttpStatus httpStatus) {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.addAll(httpHeadersMap);
    return ResponseEntity.status(httpStatus).headers(httpHeaders).body(body);
  }
}

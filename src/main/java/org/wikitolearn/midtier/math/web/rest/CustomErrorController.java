package org.wikitolearn.midtier.math.web.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.wikitolearn.midtier.math.entity.ApiError;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@ApiIgnore
public class CustomErrorController implements ErrorController {

  private static final String PATH = "/error";

  @Autowired
  private ErrorAttributes errorAttributes;

  @RequestMapping(value = PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  ResponseEntity<ApiError> error(HttpServletRequest request, HttpServletResponse response) {
    final WebRequest requestAttributes = new ServletWebRequest(request);
    final Map<String, Object> errorAttributuesMap = errorAttributes.getErrorAttributes(requestAttributes, false);
    final String message = (String) errorAttributuesMap.get("message");
    final String error = (String) errorAttributuesMap.get("error");
    
    return ResponseEntity.status(response.getStatus())
        .body(new ApiError(HttpStatus.valueOf(response.getStatus()), message, error));
  }

  @Override
  public String getErrorPath() {
    return PATH;
  }
}
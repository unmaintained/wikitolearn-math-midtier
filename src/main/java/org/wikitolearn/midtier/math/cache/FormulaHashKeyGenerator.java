package org.wikitolearn.midtier.math.cache;

import java.lang.reflect.Method;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.cache.interceptor.KeyGenerator;
import org.wikitolearn.midtier.math.entity.dto.out.StoreFormulaClientDto;

public class FormulaHashKeyGenerator implements KeyGenerator {

  @Override
  public Object generate(Object target, Method method, Object... params) {
    StoreFormulaClientDto formula = (StoreFormulaClientDto) params[0];
    String hash = DigestUtils.sha256Hex(formula.getFormula());
    return hash;    
  }

}

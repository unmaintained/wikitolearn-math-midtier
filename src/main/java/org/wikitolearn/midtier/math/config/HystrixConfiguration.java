package org.wikitolearn.midtier.math.config;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
public class HystrixConfiguration {}

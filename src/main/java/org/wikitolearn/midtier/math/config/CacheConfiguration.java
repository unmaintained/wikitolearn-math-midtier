package org.wikitolearn.midtier.math.config;

import java.util.Arrays;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wikitolearn.midtier.math.cache.FormulaHashKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {
  
  @Bean
  public CacheManager cacheManager() {
      SimpleCacheManager cacheManager = new SimpleCacheManager();
      cacheManager.setCaches(Arrays.asList(
        new ConcurrentMapCache("mathoid-checks"),
        new ConcurrentMapCache("pdf-inline-checks"),
        new ConcurrentMapCache("pdf-block-checks"),
        new ConcurrentMapCache("svgs"),
        new ConcurrentMapCache("svg-media"),
        new ConcurrentMapCache("formulas")));
      return cacheManager;
  }
  
  @Bean("formulaHashKeyGenerator")
  public KeyGenerator keyGenerator() {
      return new FormulaHashKeyGenerator();
  }
}
